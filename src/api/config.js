const mainURL = import.meta.env.VITE_API_URL;
const token = import.meta.env.VITE_API_TOKEN;

const basicOptions = {
  mode: "cors", // no-cors, *cors, same-origin
  cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
};

async function uploadImage(image) {
  const formData = new FormData();
  formData.append(image);

  try {
    const response = await fetch(`${mainURL}`, {
      ...basicOptions,
      method: "POST",
      body: formData,
    });
    if (response.ok) {
      const data = await response.json();

      return data;
    }
  } catch (error) {
    console.log(error);
  }
}

async function getImages() {
  return [];
}

async function deleteImage(id) {}

async function uploadVideo() {}

async function getVideos() {
  return [];
}

async function deleteVideo(id) {}

export {
  uploadImage,
  getImages,
  deleteImage,
  uploadVideo,
  getVideos,
  deleteVideo,
};
