import { useEffect, useRef, useState } from "react";
import { AiOutlineUpload, AiOutlineDelete } from "react-icons/ai";
import { Navigate } from "react-router-dom";
import { getVideos } from "../api/config";

function isLoggedOn() {
  return sessionStorage.getItem("user");
}

function Videos() {
  const fileInput = useRef();
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    async function listVideos() {
      const vids = await getVideos();
      setVideos(vids);
    }

    listVideos();
  }, []);

  const handleUpload = (ev) => {
    fileInput.current.click();
  };

  const handleDelete = (id) => {
    const newVideos = videos.filter((video) => video._id !== id);
    setVideos(newVideos);
  };

  if (isLoggedOn) {
    return (
      <div>
        {videos.map((video) => (
          <Video
            key={video._id}
            id={video._id}
            name={video.name}
            onDelete={handleDelete}
          />
        ))}
        <br />
        <input type='file' className='hidden' ref={fileInput} />
        <button
          className='text-white flex justify-between items-center'
          onClick={handleUpload}
        >
          Upload a video{" "}
          <span className='inline-block ml-2'>
            <AiOutlineUpload size='1.25rem' />
          </span>
        </button>
      </div>
    );
  }
}

function Video({ id, name, onDelete }) {
  return (
    <div className='p-4 m-4 flex justify-between'>
      {{ name }}
      <span onClick={() => onDelete(id)}>
        <AiOutlineDelete />
      </span>
    </div>
  );
}

export default Videos;
