import { useState, useEffect, useRef } from "react";
import { AiOutlineUpload, AiOutlineDelete } from "react-icons/ai";
import { getImages } from "../api/config";

function Images() {
  const fileInput = useRef(null);
  const [images, setImages] = useState([]);

  useEffect(() => {
    async function listVideos() {
      const imgs = await getImages();
      setImages(imgs);
    }

    listVideos();
  }, []);

  const handleUpload = () => {
    fileInput.current.click();
  };

  const handleDelete = (id) => {};

  return (
    <>
      {images.map((image) => (
        <Image key={image._id} name={image.name} />
      ))}
      <br />
      <input type='file' className='hidden' ref={fileInput} />
      <button
        className='text-white flex justify-between items-center'
        onClick={handleUpload}
      >
        Upload an image{" "}
        <span className='inline-block ml-2'>
          <AiOutlineUpload size='1.25rem' />
        </span>
      </button>
    </>
  );
}

function Image({ name }) {
  return (
    <div className='p-4 m-4 flex justify-between'>
      {{ name }}
      <span onClick={() => onDelete(id)}>
        <AiOutlineDelete />
      </span>
    </div>
  );
}

export default Images;
