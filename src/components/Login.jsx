import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const handleLogin = () => {
    sessionStorage.setItem("user", JSON.stringify({ email, password }));

    navigate("/");
  };

  return (
    <div className='mx-auto m-10 p-4'>
      <h1 className='text-5xl text-center mb-6'>Login</h1>
      <div className='flex flex-col items-center justify-center'>
        <input
          className='p-2 m-4 w-64 border border-gray-300 rounded'
          type='text'
          placeholder='Email'
          value={email}
          onChange={({ target }) => setEmail(target.value)}
        />

        <input
          className='p-2 m-4 w-64 border border-gray-300 rounded'
          type='password'
          placeholder='Password'
          value={password}
          onChange={({ target }) => setPassword(target.value)}
        />

        <button className='text-white' onClick={handleLogin}>
          Login
        </button>
      </div>
    </div>
  );
}

export default Login;
