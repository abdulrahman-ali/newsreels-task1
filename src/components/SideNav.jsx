import { Link } from "react-router-dom";

function SideNav(props) {
  return (
    <div className='w-1/4 bg-blue-200 divide-y divide-solid'>
      <div className='p-4 my-10 text-center text-2xl'>
        <Link to='/videos'>Videos</Link>
      </div>
      <div className='p-4 my-10 text-center text-2xl'>
        <Link to='/images'>Images</Link>
      </div>
    </div>
  );
}

export default SideNav;
