// import SideNav from "./components/SideNav";
import "./App.css";
import { Routes, Route, Navigate } from "react-router-dom";
import Login from "./components/Login";
import Videos from "./components/Videos";
import Images from "./components/Images";

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/home' element={<Navigate to={"/"} replace />} />
        <Route path='/videos' element={<Videos />} />
        <Route path='/images' element={<Images />} />
        <Route
          path='*'
          element={<h1 style={{ color: "#333333" }}>404 | Not found</h1>}
        />
      </Routes>
    </>
  );
}

export default App;
// b4xZiiXG3qRE8pC
